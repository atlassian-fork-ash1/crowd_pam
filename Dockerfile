FROM ubuntu:latest
MAINTAINER Zach Boody <zboody@atlassian.com>

RUN apt-get install -y curl
CMD ["curl", "--fail", "http://Crowd:8095/crowd"]
